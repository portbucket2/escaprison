﻿Shader "Unlit/Outline"
{
    Properties
    {
		_OutlineAmount("Outline amount", Range(0.000001, 3)) = 1
		_Color("Outline Color", color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

       

		Pass
		{
			Cull Front
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
							// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				UNITY_FOG_COORDS(0)
				float4 vertex : SV_POSITION;
			};

			float _OutlineAmount;
			fixed4 _Color;

			v2f vert(appdata v)
			{
				v2f o;
				float4 vert = v.vertex;
				vert.xyz = normalize(v.normal) * (_OutlineAmount)+v.vertex.xyz;

				o.vertex = UnityObjectToClipPos(vert);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _Color;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
				ENDCG
		}
    }
}
