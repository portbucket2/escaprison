﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sickey : Key, Ipickable
{

    public float streetcred = 500;

    public Animator sickanimator;
    Collider col;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider> ();

        interactiondecreaserate = interactiontriggercap / interactiontime;
        interactionincreaserate = interactiondecreaserate * 2;
    }

    // Update is called once per frame
    void Update()
    {
        interactiontriggeramount -= Time.deltaTime * interactiondecreaserate;

        interactiontriggeramount = Mathf.Clamp ( interactiontriggeramount, 0, interactiontriggercap + 1 );
    }

    public override void onpickup ()
    {
        CharController.instance.keylist.Add ( this );
        col.enabled = false;
    }

    public void Help ()
    {
        CharController.instance.increaseRep ( streetcred );
        sickanimator.SetBool ("sick",false);
        col.enabled = false;
    }


    public override void onInteract ()
    {
        LockUI.instance.activatethisUI ( "Help this guy or take his key?", () =>{ return "Help"; },() => { return "Take Key";}, () =>
        {
            Help ();
            LockUI.instance.deactivatethisUI ();
        }, () =>
        {
            //Debug.Log ( "button 2 pressed with functionality derived from " + name );
            onpickup ();
            LockUI.instance.deactivatethisUI ();
        } );
    }
}
