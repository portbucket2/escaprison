﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : Lock
{
    public bool oneway = false;

    public LinkedPoint entrance;
    public LinkedPoint exit;

    // Start is called before the first frame update
    void Start()
    {
        if ( entrance.otherLinkedPoints.Contains ( exit ) )
        {
            entrance.otherLinkedPoints.Remove ( exit );
        }

        if ( exit.otherLinkedPoints.Contains ( entrance ) )
        {
            exit.otherLinkedPoints.Remove ( entrance );
        }

        interactiondecreaserate = interactiontriggercap / interactiontime;
        interactionincreaserate = interactiondecreaserate * 2;
    }

    // Update is called once per frame
    void Update()
    {
        interactiontriggeramount -= Time.deltaTime * interactiondecreaserate;

        interactiontriggeramount = Mathf.Clamp ( interactiontriggeramount, 0, interactiontriggercap + 1 );

        counttowardsdeactivation--;

        if ( counttowardsdeactivation <= 0 )
        {
            counttowardsdeactivation = 0;
            outline.gameObject.SetActive ( false );
        }
    }

    public void opensesame ()
    {
        if ( !entrance.otherLinkedPoints.Contains ( exit ) )
        {
            entrance.otherLinkedPoints.Add ( exit );
        }

        if ( !oneway )
        {
            if ( !exit.otherLinkedPoints.Contains ( entrance ) )
            {
                exit.otherLinkedPoints.Add ( entrance );
            }
        }
    }

    public override void afterunlock ()
    {
        opensesame();

        gameObject.SetActive ( false );
    }
}
