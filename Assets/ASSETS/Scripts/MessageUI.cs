﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageUI : MonoBehaviour
{
    public static MessageUI instance;

    public Button affirmativebutton; 
    public Text messageBody; 
    public Text buttontext; 
    // Start is called before the first frame update
    void Start()
    {
        DeactivateMessage ();
    }

    private void Awake ()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Activatemessage (string messagebody, System.Action onOK)
    {
        affirmativebutton.gameObject.SetActive ( onOK != null );

        gameObject.SetActive ( true );
        messageBody.text = messagebody;

        affirmativebutton.onClick.RemoveAllListeners ();
        affirmativebutton.onClick.AddListener ( () => onOK?.Invoke () );

        StartCoroutine ( actafter ( onOK, 1f ) );
    }

    IEnumerator actafter ( System.Action actn, float tim )
    {
        yield return new WaitForSeconds ( tim );

        actn?.Invoke ();

        DeactivateMessage ();

        affirmativebutton.gameObject.SetActive ( false );
    }

    void DeactivateMessage ()
    {
        gameObject.SetActive ( false );
    }
}
