﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UImanager : MonoBehaviour
{
    public static UImanager instance;

    public Image Circlebar;

    int counttowardsdeactivation;

    public Text framecounter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        framecounter.text = ( 1 / ( Time.deltaTime ) ).ToString ();
        counttowardsdeactivation--;

        if ( counttowardsdeactivation <= 0 )
        {
            counttowardsdeactivation = 0;
            deactivatecirclebar ();
        }
    }

    private void Awake ()
    {
        instance = this;
    }

    public void activatecirclebar (float fill)
    {
        counttowardsdeactivation = 2;
        Circlebar.gameObject.SetActive ( true );
        Circlebar.fillAmount = fill;
    }
    public void deactivatecirclebar ()
    {
        Circlebar.gameObject.SetActive ( false );
        
    }
}
