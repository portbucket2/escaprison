﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Lock : MonoBehaviour, Iinteractible
{

    int tapsrequired = 10;
    int tapsmade = 0;

    public List<Key> unlockers;

    bool locked;

    public bool pickable = true;

    public float interactiontime = 2;
    public float interactiontriggeramount;
    public float interactiontriggercap = 4;


    public float interactionincreaserate;
    public float interactiondecreaserate;

    public GameObject outline;

    public int counttowardsdeactivation;

    public bool unlockthis (Key k)
    {
        if ( unlockers.Contains ( k ) )
        {
            locked = false;

            afterunlock ();

            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void afterunlock ()
    {
        
    }

    void lockthis ()
    {
        locked = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //bool Playerhasthiskey ()
    //{
    //    foreach ( var v in unlockers )
    //    {
    //        if ( CharController.instance.keylist.Contains ( v ) )
    //        {
    //            return true;
    //        }
    //    }

    //    return false;
    //}

    public void Interact ()
    {
        counttowardsdeactivation = 2;
        outline.gameObject.SetActive ( true );
        interactiontriggeramount += Time.deltaTime * interactionincreaserate;
        if ( interactiontriggeramount < interactiontriggercap )
        {
            UImanager.instance.activatecirclebar ( interactiontriggeramount / interactiontriggercap );
        }
        else
        {
            onInteract ();
        }
    }

    void onInteract ()
    {
        System.Func<string> func1 = null;
        System.Func<string> func2 = null;
        if ( CharController.instance.keylist.AsQueryable ().Intersect ( unlockers ).Any () )
        {
            func1 = () =>
            {
                return "Use Key";
            };
        }
        
        if ( pickable )
        {
            func2 = () =>
            {
                return "Use Skill to tap " + ( tapsrequired - tapsmade );
            };
        }
        

        LockUI.instance.activatethisUI ( "Unlock?",func1, func2,  () =>
        {
            foreach ( Key k in CharController.instance.keylist )
            {
                if ( unlockthis ( k ) )
                {
                    LockUI.instance.deactivatethisUI ();
                }

            }
        }, () =>
        {

            //Debug.Log ( "button 2 pressed with functionality derived from " + name );
            tapsmade++;
            //Debug.Log ( tapsmade + " taps made " + tapsrequired + " taps required" );

            if ( tapsmade > tapsrequired )
            {

                locked = false;

                afterunlock ();

                LockUI.instance.deactivatethisUI ();
            }
        } );
    }
}
