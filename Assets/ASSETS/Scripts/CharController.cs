﻿using FRIA;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharController : MonoBehaviour
{
    public static CharController instance;

    public PathChoiceUI pathui;

    LinkedPoint destination;
    public LinkedPoint origin;
    LinkedPoint next;

    bool alive = true;

    [SerializeField] float movementspeed = 1;
    [SerializeField] float twosteplength = 1;
    [SerializeField] float minDistDelta = 0.01f;
    [SerializeField] float closeDistDelta = 1f;

    [SerializeField] LayerMask interactiblelayer;

    Direction defaultdir = Direction.FRONT;

    public List<Key> keylist;

    Vector3 prevpos;

    [SerializeField] Transform head;
    [SerializeField] Animator headanimator;

    [SerializeField] Camera maincamera;

    Vector4 limits = new Vector4 ( 90, -90, 40, -40 );

    [SerializeField]float raycastrange = 50;

    public HardData<float> reputation;

    Iinteractible lasthitinteractible;

    [SerializeField] float framerate;

    // Start is called before the first frame update
    void Start ()
    {
        destination = origin.otherLinkedPoints[0];
        transform.position = origin.transform.position;

        prevpos = transform.position - 0.01f * ( destination.transform.position - origin.transform.position ).normalized;

        reputation = new HardData<float> ( "REPUTATION", 0 );

        headanimator.speed = twosteplength;//movementspeed / twosteplength;

    }

    private void Awake ()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update ()
    {
        //if ( Input.GetKey ( KeyCode.W ) )
        //{
        //    progressTowardNextPoint ( Direction.FRONT );
        //}
        //else if ( Input.GetKey ( KeyCode.A ) )
        //{
        //    progressTowardNextPoint ( Direction.LEFT );
        //}
        //else if ( Input.GetKey ( KeyCode.D ) )
        //{
        //    progressTowardNextPoint ( Direction.RIGHT );
        //}
        framerate = 1 / Time.deltaTime;

        if ( Input.GetMouseButton ( 0 ) && !holdallbehavior )
        {
            headanimator.enabled = true;
            progressTowardNextPoint ();

            interactionloop ();
        }
        else
        {
            headanimator.enabled = false;
        }



        transform.rotation = Quaternion.Slerp ( transform.rotation, Quaternion.LookRotation ( transform.position - prevpos, Vector3.up ), 0.1f );
        //transform.rotation =  Quaternion.LookRotation ( transform.position - prevpos, Vector3.up );

        cameracontrol ();

    }

    void interactionloop ()
    {
        Ray rey = maincamera.ScreenPointToRay ( new Vector3 ( Screen.width / 2f, Screen.height / 2f, 0 ) );
        //Debug.DrawRay ( rey.origin,rey.direction*5000 );
        RaycastHit hit;
        if ( Physics.Raycast ( rey, out hit, raycastrange, interactiblelayer ) )
        {

            Iinteractible inter = hit.collider.GetComponent<Iinteractible> ();

            if ( inter != null )
            {

                //Debug.Log ( "hitting" );
                if ( lasthitinteractible == inter )
                {
                    inter.Interact ();
                }
                else
                {
                    LockUI.instance.deactivatethisUI ();

                    lasthitinteractible = inter;

                    inter.Interact ();
                }
            }
            else
            {            
                LockUI.instance.deactivatethisUI ();
                lasthitinteractible = null;
            }
        }
        else
            LockUI.instance.deactivatethisUI ();
    }



    void checkAngle ( float angle, bool EnblNOTupdt, bool select )
    {
        LinkedPoint[] points = destination.GetRelativePoint ( origin );

        if ( points != null )
        {
            if ( !points[1] )
            {
                if ( !points[0] )
                {
                    if ( !points[2] )
                    {
                        // no paths
                        if ( select )
                        {
                            next = null;
                        }
                    }
                    else
                    {
                        //only right
                        if ( select )
                        {
                            next = points[2];
                        }
                    }
                }
                else
                {
                    if ( !points[2] )
                    {
                        // only left
                        if ( select )
                        {
                            next = points[0];
                        }
                    }
                    else
                    {
                        // both left and right

                        if ( EnblNOTupdt )
                        {
                            pathui.enableWithButtons ( points[0] != null, points[1] != null, points[2] != null );
                        }

                        Direction d = angle > Mathf.Lerp ( limits.y, limits.x, 0.5f ) ? Direction.LEFT : Direction.RIGHT;
                        pathui.enableHighlight ( (int) d );
                        if ( select )
                        {
                            next = points[(int) d];
                            Debug.Log ( "turned " + d + " for angle " + angle );
                        }
                    }
                }
            }
            else
            {
                if ( !points[0] )
                {
                    if ( !points[2] )
                    {
                        // only forward

                        if ( select )
                        {
                            next = points[1];
                        }
                    }
                    else
                    {
                        // both right and forward

                        if ( EnblNOTupdt )
                        {
                            pathui.enableWithButtons ( points[0] != null, points[1] != null, points[2] != null );
                        }

                        Direction d = angle > Mathf.Lerp ( limits.y, limits.x, 0.4f ) ? Direction.FRONT : Direction.RIGHT;
                        pathui.enableHighlight ( (int) d );
                        if ( select )
                        {
                            next = points[(int) d];
                            Debug.Log ( "turned " + d + " for angle " + angle );
                        }
                    }
                }
                else
                {
                    if ( !points[2] )
                    {
                        // both left and forward

                        if ( EnblNOTupdt )
                        {
                            pathui.enableWithButtons ( points[0] != null, points[1] != null, points[2] != null );
                        }

                        Direction d = angle > Mathf.Lerp ( limits.y, limits.x, 0.6f ) ? Direction.LEFT : Direction.FRONT;
                        pathui.enableHighlight ( (int) d );
                        if ( select )
                        {
                            next = points[(int) d];
                            Debug.Log ( "turned " + d + " for angle " + angle );
                        }
                    }
                    else
                    {
                        // all three paths

                        if ( EnblNOTupdt )
                        {
                            pathui.enableWithButtons ( points[0] != null, points[1] != null, points[2] != null );
                        }

                        Direction d = angle > Mathf.Lerp ( limits.y, limits.x, 2f / 3f ) ? Direction.LEFT : angle > Mathf.Lerp ( limits.y, limits.x, 1f / 3f ) ? Direction.FRONT : Direction.RIGHT;
                        pathui.enableHighlight ( (int) d );
                        if ( select )
                        {
                            next = points[(int) d];
                            Debug.Log ( "turned " + d + " for angle " + angle );
                        }
                    }
                }
            }


        }
        else if ( select )
        {
            next = null;
        }
    }

    void check ( Direction d )
    {
        LinkedPoint[] points = destination.GetRelativePoint ( origin );

        if ( points != null )
        {
            //Debug.Log ( "point 0 is null?  " + ( points[0] == null ).ToString () );
            //Debug.Log ( "point 1 is null?  " + ( points[1] == null ).ToString () );
            //Debug.Log ( "point 2 is null?  " + ( points[2] == null ).ToString () );
            if ( d == Direction.FRONT )
            {
                if ( points[1] )
                {
                    next = points[1];
                }
                else if ( points[2] )
                {
                    next = points[2];
                }
                else if ( points[0] )
                {
                    next = points[0];
                }
                else
                {
                    next = null;
                }
            }
            else if ( d == Direction.RIGHT )
            {
                if ( points[2] )
                {
                    next = points[2];
                }
                else if ( points[1] )
                {
                    next = points[1];
                }
                else if ( points[0] )
                {
                    next = points[0];
                }
                else
                {
                    next = null;
                }
            }
            else
            {
                if ( points[0] )
                {
                    next = points[0];
                }
                else if ( points[1] )
                {
                    next = points[1];
                }
                else if ( points[2] )
                {
                    next = points[2];
                }
                else
                {
                    next = null;
                }
            }
        }
        else
            next = null;


    }

    bool close = false;

    void progressTowardNextPoint ()
    {
        float angle = head.localEulerAngles.y;

        while ( angle < -180 || angle >= 180 )
        {
            angle -= Mathf.Sign ( angle ) * 360;
        }

        angle = Mathf.Clamp ( angle, limits.y, limits.x );

        if ( ( transform.position - destination.transform.position ).magnitude > minDistDelta )
        {
            if ( ( transform.position - destination.transform.position ).magnitude > closeDistDelta )
            {
                close = false;
            }
            else
            {


                checkAngle ( angle, !close, false );

                if ( !close )
                {
                    close = true;
                }

            }


            prevpos = transform.position;
            transform.position = Vector3.MoveTowards ( transform.position, destination.transform.position, movementspeed * Time.deltaTime *Mathf.Lerp(1,0.5f,( Mathf.Abs ( head.localEulerAngles.y ) ) /(limits.x)) );
        }
        else
        {
            if ( !externalSelectionDone )
            {
                checkAngle ( angle, false, true );
            }
            else
            {
                externalSelectionDone = false;
            }
            swap ();

            pathui.closewindow ();
        }

    }

    void swap ()
    {
        if ( next )
        {
            origin = destination;
            destination = next;
        }
        else
        {
            if ( destination.endpoint )
            {
                Debug.Log ( "LevelComplete" );
                //GameOver ( true );

                MessageUI.instance.Activatemessage ( "LevelComplete", () => {
                    GameOver ( true );

                } );
                holdallbehavior = true;
            }
        }
    }

    Vector3 originalpos;
    Vector2 originalcampos;

    [SerializeField] float scaling = 100;

    void cameracontrol ()
    {
        if ( Input.GetMouseButtonDown ( 0 ) )
        {
            originalpos = Input.mousePosition;
            originalcampos = new Vector2 ( head.localEulerAngles.x, head.localEulerAngles.y );
        }
        else if ( Input.GetMouseButton ( 0 ) )
        {
            float changedX = ( originalcampos.x + scaling * ( Input.mousePosition - originalpos ).y );  //limits.y,limits.x);
            float changedY = ( originalcampos.y - scaling * ( Input.mousePosition - originalpos ).x ); //limits.w,limits.z);

            while ( changedX < -180 || changedX >= 180 )
            {
                changedX -= Mathf.Sign ( changedX ) * 360;
            }

            while ( changedY < -180 || changedY >= 180 )
            {
                changedY -= Mathf.Sign ( changedY ) * 360;
            }

            changedX = Mathf.Clamp ( changedX, limits.w, limits.z );
            changedY = Mathf.Clamp ( changedY, limits.y, limits.x );

            head.localEulerAngles = new Vector3 ( changedX, changedY, head.localEulerAngles.z );
        }
    }

    float rangeof180 ( float angle )
    {
        while ( angle < -180 || angle >= 180 )
        {
            angle -= Mathf.Sign ( angle ) * 360;
        }

        return angle;
    }

    bool externalSelectionDone = false;

    public void externalSelection ( Direction d )
    {
        externalSelectionDone = true;

        check ( d );
    }

    public float CheckPlayerPlacementBetweenPoints ( LinkedPoint origin, LinkedPoint destination )
    {
        LinkedPoint playerorigin = this.origin;
        LinkedPoint playerdestination = this.destination;

        if ( playerdestination && playerorigin && ( ( origin == playerorigin && destination == playerdestination ) || ( origin == playerdestination && destination == playerorigin ) ) )
        {
            return ( origin.transform.position - transform.position ).magnitude;
        }
        else
            return -1;
    }

    public bool ISPlayerNearHere ( LinkedPoint Here, float near )
    {
        return ( Here.transform.position - transform.position ).magnitude < near;
    }

    public void IkillU ( NPCBehavior I )
    {
        if ( !holdallbehavior )
        {
            holdallbehavior = true;
            float chance = Random.Range ( 1f,0f );
            if (  chance > I.difficulty )
            {
                MessageUI.instance.Activatemessage ( "Fight Happened, You Lost", () =>
                {
                    holdallbehavior = false;
                    GameOver ( false );
                    
                } );
                
            }
            else
            {
                
                MessageUI.instance.Activatemessage ( "Fight Happened. You Won", ()=>
                {
                    holdallbehavior = false;
                    I.die ();
                } );
            }
        }
    }

    public void killmepls ( NPCBehavior me )
    {
        if ( !holdallbehavior )
        {
            holdallbehavior = true;
            MessageUI.instance.Activatemessage ( "you stealth killed this guard", ()=>
            {
                holdallbehavior = false;
                me.die ();
            } );
        }
    }

    public void increaseRep (float amount)
    {
        reputation.value += amount;

        MessageUI.instance.Activatemessage ( "Reputation increased by " + amount + " . Rep is now " + reputation.value,null );
    }

    bool gameover;
    bool interactive = true;
    public bool holdallbehavior;

    void GameOver ( bool won )
    {
        Debug.Log ( won ? "YOU WIN" : "you lose" );

        SceneManager.LoadScene ( SceneManager.GetActiveScene ().buildIndex + (won?1:0) );
    }
}
