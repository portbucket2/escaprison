﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedPoint : MonoBehaviour
{

    public bool endpoint = false;

    public List<LinkedPoint> otherLinkedPoints;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public LinkedPoint[] GetRelativePoint (LinkedPoint originalPoint)
    {
        if (  otherLinkedPoints.Count<1 )//&& !otherLinkedPoints.Contains ( originalPoint )  )
        {
            return null;
        }

        float minangle = float.MaxValue, maxplusangle = float.MinValue, maxminusangle = float.MaxValue;

        LinkedPoint targetmin= null, targetmaxplus = null, targetmaxminus = null;

        foreach ( LinkedPoint lp in otherLinkedPoints )
        {
            if ( lp != originalPoint )
            {
                float sa = Vector3.SignedAngle ( transform.position - originalPoint.transform.position, lp.transform.position - transform.position, Vector3.up );
                //Debug.Log ( lp.name + " gave angle " + sa );
                if ( sa < maxminusangle )
                {
                    maxminusangle = sa;
                    targetmaxminus = lp;
                }

                if ( sa > maxplusangle )
                {
                    maxplusangle = sa;
                    targetmaxplus = lp;
                }

                if ( Mathf.Abs ( sa ) < minangle )
                {
                    minangle = Mathf.Abs ( sa );

                    targetmin = lp;
                }
            }
            else
            {
                //Debug.Log ( lp.name + " is THE reference point" );
            }
        }

        //Debug.Log ( "max minus angle is " + maxminusangle + " max plus angle is " + maxplusangle + " minangle is " + minangle );

        LinkedPoint[] returnee = new LinkedPoint[3];

        returnee[0] = maxminusangle < -45 ? targetmaxminus : null;
        returnee[2] = maxplusangle > 45 ? targetmaxplus : null;
        returnee[1] = minangle < 45 ? targetmin : null;

        return returnee;
    }

    private void OnDrawGizmos ()
    {
        if ( otherLinkedPoints != null )
        {
            foreach ( LinkedPoint lp in otherLinkedPoints )
            {
                Gizmos.DrawLine ( transform.position, lp.transform.position );
            }
        }
        
    }
}

public enum Direction
{
    LEFT=0,FRONT=1,RIGHT=2
}
