﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockUI : MonoBehaviour
{
    public static LockUI instance;

    [SerializeField] Text textmain;
    [SerializeField] Text textbutton1;
    [SerializeField] Text textbutton2;
    [SerializeField] Button button1;
    [SerializeField] Button button2;

    Iinteractible callerofthisUI;

    private void Awake ()
    {
        instance = this;

        deactivatethisUI ();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void nullifycaller ()
    {
        callerofthisUI = null;
    }

    public void activatethisUI ( string maintext, System.Func<string> buttontext1, System.Func<string> buttontext2, System.Action onbutton1, System.Action onbutton2 )
    {
        

        gameObject.SetActive ( true );

        textmain.text = maintext;

        if (  buttontext1 != null )
        {
            button1.interactable = true;
            textbutton1.text = buttontext1();
            button1.onClick.RemoveAllListeners ();
            button1.onClick.AddListener ( () =>
            {
                onbutton1?.Invoke ();
                textbutton1.text = buttontext1 ();
            } );
        }
        else
        {
            button1.interactable = false;
            textbutton1.text = "No Key";
        }


        if ( buttontext2 != null )
        {
            button2.interactable = true;
            textbutton2.text = buttontext2();
            button2.onClick.RemoveAllListeners ();
            button2.onClick.AddListener ( () =>
            {
                onbutton2?.Invoke ();
                textbutton2.text = buttontext2 ();
            } );
        }
        else
        {
            button2.interactable = false;
            textbutton2.text = "";
        }


    }

    public bool haveIalreadycalled ( Iinteractible I )
    {
        return I == callerofthisUI;
    }

    public void deactivatethisUI ()
    {
        gameObject.SetActive ( false );
    }
}
