﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathChoiceUI : MonoBehaviour
{
    public CharController CC;

    public Image[] selectorHighlights;
    public Button[] selectors;

    // Start is called before the first frame update
    void Start ()
    {
        for ( int i = 0; i < 3; i++ )
        {
            int j = i;
            selectors[i].onClick.AddListener ( () => directionselected ( j ) );
        }

        closewindow ();
    }

    // Update is called once per frame
    void Update ()
    {

    }

    public void enableWithButtons ( bool left, bool front, bool right )
    {
        gameObject.SetActive ( true );
        for ( int i = 0; i < 3; i++ )
        {
            selectorHighlights[i].enabled = false;
        }

        selectors[0].gameObject.SetActive ( left );
        selectors[1].gameObject.SetActive ( front );
        selectors[2].gameObject.SetActive ( right );
    }

    public void closewindow ()
    {
        gameObject.SetActive ( false );

        for ( int i = 0; i < 3; i++ )
        {
            selectorHighlights[i].enabled = false;
            selectors[i].gameObject.SetActive ( false );
        }
    }

    public void enableHighlight ( int index )
    {
        if ( index >= 0 && index < 3 )
        {
            if ( selectors[index].gameObject.activeSelf )
            {
                for ( int i = 0; i < 3; i++ )
                {
                    selectorHighlights[i].enabled = i == index;
                }
            }
        }
    }

    void directionselected ( int directionIndex )
    {
        CC.externalSelection ( (Direction) directionIndex );
        closewindow ();

        //why
    }
}
