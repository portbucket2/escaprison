﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour,Ipickable
{
    public Lock[] unlockables;

    public float interactiontime = 2;
    public float interactiontriggeramount;
    public float interactiontriggercap = 4;

    public float interactionincreaserate;
    public float interactiondecreaserate;

    public GameObject outline;

    public int counttowardsdeactivation;

    // Start is called before the first frame update
    void Start()
    {
        interactiondecreaserate = interactiontriggercap / interactiontime;
        interactionincreaserate = interactiondecreaserate * 2;
    }

    // Update is called once per frame
    void Update()
    {
        interactiontriggeramount -= Time.deltaTime * interactiondecreaserate;

        interactiontriggeramount = Mathf.Clamp ( interactiontriggeramount, 0, interactiontriggercap + 1 );

        counttowardsdeactivation--;

        if ( counttowardsdeactivation <= 0 )
        {
            counttowardsdeactivation = 0;
            outline.gameObject.SetActive ( false );
        }
    }

    public void pickup ()
    {
        onpickup ();
    }

    public virtual void onpickup ()
    {
        CharController.instance.keylist.Add ( this );

        gameObject.SetActive ( false );
    }

    public void Interact ()
    {
        counttowardsdeactivation = 2;
        outline.gameObject.SetActive ( true );
        interactiontriggeramount += Time.deltaTime * interactionincreaserate;
        if ( interactiontriggeramount < interactiontriggercap )
        {
            UImanager.instance.activatecirclebar ( interactiontriggeramount / interactiontriggercap );
        }
        else
        {
            onInteract ();
        }
        
    }

    public virtual void onInteract ()
    {
        LockUI.instance.activatethisUI ( "pick up key?",()=>
        {
            return "Pick Up";
        },()=>
        {
            return "Walk Away";
        } , () =>
        {
            pickup ();
            LockUI.instance.deactivatethisUI ();
        }, () =>
        {
            //Debug.Log ( "button 2 pressed with functionality derived from " + name );
            LockUI.instance.deactivatethisUI ();
        } );
    }
}

public interface Ipickable : Iinteractible
{
    void pickup ();
}

public interface Iinteractible
{
    void Interact ();
}
