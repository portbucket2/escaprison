﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arifscript2 : MonoBehaviour
{
    float a, b, c, m, n;
    public float R, P, S;
    public float initA, initB, initC, ininM, initN;

    float sqrt2;

    // Start is called before the first frame update
    void Start()
    {
        sqrt2 = Mathf.Sqrt ( 2 );

        m = ininM - 1;
        n = initN - 1;

        float newM = m + 1;
        float newN = n + 1;

        int numberiterations = 0;

        while ( !(  ( Mathf.Abs ( newM - m ) < 0.00001f ) && ( Mathf.Abs ( newN - n ) < 0.00001f ) ) && numberiterations<1000000 )
        {
            m = newM;
            n = newN;

            float w = jacobian2x2element ( false, false );
            float x = jacobian2x2element ( false, true );
            float y = jacobian2x2element ( true, false );
            float z = jacobian2x2element ( true, true );

            float iw = z / ( w * z - x * y );
            float ix = w / ( w * z - x * y );
            float iy = -y / ( w * z - x * y );
            float iz = -x / ( w * z - x * y );

            float f1 = returnfunc ( false );
            float f2 = returnfunc ( true );

            float sol1 = iw * f1 + ix * f2;
            float sol2 = iy * f1 + iz * f2;

            newM = m - sol1;
            newN = n - sol2;

            numberiterations++;
        }

        m = newM;
        n = newN;
        a = ( n - sqrt2 * R ) / ( ( m * m ) - ( m * ( 1 + sqrt2 * R ) ) + ( R * R / 2 ) );
        b = -1 - ( sqrt2 * R * a );
        c = ( R * R * a * 0.5f ) + ( sqrt2 * R );



        Debug.Log ( "A = " + a + " , B = " + b + " , C = " + c + " , M = " + m + " , N = " + n + " , number of iterations required = " + numberiterations );

        Debug.Log ( "circle check " + ( ( m - P ) * ( m - P ) + n * n - S * S ).ToString () );

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    float jacobian2x2element ( bool row, bool column )
    {
        if ( !row )
        {
            if ( !column )
            {
                return (n-sqrt2 * R)*((( ( ( m * m ) - ( m * ( 1 + sqrt2 * R ) ) + ( R * R / 2 ) ) *(4*m - 2*P +sqrt2*R)) -((2*m-sqrt2*R-1)*((2*m*m)-m*(2*P-sqrt2*R)+sqrt2*R*P)))/( ( ( m * m ) - ( m * ( 1 + sqrt2 * R ) ) + ( R * R / 2 ) ) * ( (m * m) - (m * ( 1 + sqrt2 * R )) + ( R * R / 2 ) ) ) ) -1;
            }
            else
            {
                return ( ( ( 2 * m * m ) - m * ( 2 * P - sqrt2 * R ) + sqrt2 * R * P ) / ( ( m * m ) - ( m * ( 1 + sqrt2 * R ) ) + ( R * R / 2 ) ) ) +1;
            }
        }
        else
        {
            if ( !column )
            {
                return 2*(m-P);
            }
            else
            {
                return 2*n;
            }
        }
    }

    float returnfunc ( bool row )
    {
        if ( row )
        {
            return (m-P)*(m-P) + n*n - S*S;
        }
        else
        {
            return (n-sqrt2*R)* ( ( ( 2 * m * m ) - m * ( 2 * P - sqrt2 * R ) + sqrt2 * R * P ) / ( ( m * m ) - ( m * ( 1 + sqrt2 * R ) ) + ( R * R / 2 ) ) ) + n+P-m;
        }
    }

    

    
}
