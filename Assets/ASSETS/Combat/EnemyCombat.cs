﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : MonoBehaviour
{
    public List<ComboWrapper> movelist;

    public PlayerCombat player;

    List<Movename> cuedmoves;

    Animator animator;

    Dictionary<Movename, string> triggerdic;

    public bool interrupted;

    void nextattack ()
    {
        
        int j = Random.Range ( 0, movelist.Count );
        Debug.Log ( "next attack is " + (j) );
        float t = Random.Range ( 2f, 4f );
        StartCoroutine ( ComboQueueing ( movelist[j], t ) );
    }

    public void attacked ( Animator anim, AnimatorStateInfo animstate, int layeridx)
    {
        if ( !interrupted )
        {
            if ( cuedmoves.Count > 0 )
            {
                initiatemove ( cuedmoves[0] );
                cuedmoves.RemoveAt ( 0 );
            }
            else
            {
                nextattack ();
            }
        }
    }

    IEnumerator ComboQueueing ( ComboWrapper comb, float later )
    {
        yield return new WaitForSeconds (later);

        CueCombo ( comb );
    }


    public void CueCombo ( ComboWrapper currentcombo )
    {
        cuedmoves.Clear ();
        for ( int i = 0; i < currentcombo.combo.Length; i++ )
        {
            if ( i == 0 )
            {
                initiatemove ( currentcombo.combo[i] );
            }
            else
            {
                cuedmoves.Add ( currentcombo.combo[i] );
            }
        }
    }

    void initiatemove ( Movename move )
    {
        animator.SetTrigger ( triggerdic[move] );

        Debug.Log ( "trigger set for " + move );
    }

    public void gethit ()
    {
        if ( !interrupted )
        {
            interrupted = true;

            cuedmoves.Clear ();
            animator.SetTrigger ( "hit" );
            animator.ResetTrigger ( "roundkick" );
            animator.ResetTrigger ( "sidekick" );
            animator.ResetTrigger ( "uppercut" );
            animator.ResetTrigger ( "punch" );
            animator.ResetTrigger ( "hook" );
            animator.ResetTrigger ( "cross" );

            StopAllCoroutines ();
            nextattack ();
        }
    }

    public void die ()
    {
        interrupted = true;

        cuedmoves.Clear ();
        animator.SetBool ( "die",true );
        animator.ResetTrigger ( "roundkick" );
        animator.ResetTrigger ( "sidekick" );
        animator.ResetTrigger ( "uppercut" );
        animator.ResetTrigger ( "punch" );
        animator.ResetTrigger ( "hook" );
        animator.ResetTrigger ( "cross" );

        StopAllCoroutines ();
    }

    public void win ()
    {
        interrupted = true;

        cuedmoves.Clear ();
        animator.ResetTrigger ( "roundkick" );
        animator.ResetTrigger ( "sidekick" );
        animator.ResetTrigger ( "uppercut" );
        animator.ResetTrigger ( "punch" );
        animator.ResetTrigger ( "hook" );
        animator.ResetTrigger ( "cross" );

        StopAllCoroutines ();
    }

    public void hitfinished ( Animator anim, AnimatorStateInfo animstate, int layeridx )
    {
        interrupted = false;
        
    }

    // Start is called before the first frame update
    void Start()
    {
        onstart ();
    }

    void onstart ()
    {
        animator = GetComponent<Animator> ();
        triggerdic = new Dictionary<Movename, string> ();
        triggerdic.Add ( Movename.ROUNDKICK, "roundkick" );
        triggerdic.Add ( Movename.SIDEKICK, "sidekick" );
        triggerdic.Add ( Movename.UPPERCUT, "uppercut" );
        triggerdic.Add ( Movename.PUNCH, "punch" );
        triggerdic.Add ( Movename.HOOK, "hook" );
        triggerdic.Add ( Movename.CROSS, "cross" );
        cuedmoves = new List<Movename> ();

        nextattack ();
    }


    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKeyDown ( KeyCode.Space ) )
        {
            gethit ();
        }
    }
}
[System.Serializable]
public class ComboWrapper
{
    public Movename[] combo;
}

public enum Movename
{
    ROUNDKICK,SIDEKICK,UPPERCUT,PUNCH,HOOK,CROSS
}
