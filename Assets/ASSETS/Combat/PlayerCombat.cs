﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCombat : MonoBehaviour
{
    public static PlayerCombat instance;

    Animator animator;

    public float parrywindowmultiplier = 1f;

    public float multmultiplyer = 0.707f;
    public float multgainrate = 1;

    public EnemyCombat enemy;

    public timingwindowfeedback timer;

    public bool blocking = false;

    bool attackwindow;

    public bool overrideparrywindow = false;
    public float parrywindow = 0.15f;

    public bool slowmo;
    public bool counterslowmo;

    public UnityEngine.UI.Image playerhealth;
    public UnityEngine.UI.Image enemyhealth;
    

    public float playerHP;
    public float playercurrentHP;
    public float enemyHP;
    public float enemycurrentHP;
    public float playerDmg;
    public float enemyDmg;

    public bool showripostetiming = false;

    public bool gameover = false;

    public void newparrytimeset (float time)
    {
        timer.setnewparrytime ( time );
    }

    public void AttackWindowOpen ( Animator animtr, AnimatorStateInfo animstate, float timeremaining )
    {
        //Debug.Log ( "counter chance" );
        timer.starttimer ( timeremaining );
        attackwindow = true;
        if(slowmo)Time.timeScale = 0.3f;
    }

    public void AttackOccured ( Animator animtr, AnimatorStateInfo animstate )
    {
        Debug.Log ( "attacked" );
        if ( !gameover )
        {
            if ( !blocking )
            {
                gothit ();
            }
            else
            {
                blockedhit ();
            }
        }
        attackwindow = false;
        Time.timeScale = 1f;
    }

    void gothit ()
    {
        animator.SetTrigger ( "hit" );
        Debug.Log ( "got hit" );
        playercurrentHP -= enemyDmg;
    }

    void blockedhit ()
    {
        animator.SetTrigger ( "blockhit" );
        Debug.Log ( "blocked attack" );
    }

    public void counterstarted ( Animator animtr, AnimatorStateInfo animstate )
    {
        if ( counterslowmo )
        {
            Time.timeScale = 0.3f;
        }
    }

    public void counterended ( Animator animtr, AnimatorStateInfo animstate )
    {
        if ( counterslowmo )
        {
            Time.timeScale = 1f;
        }
    }
    // Start is called before the first frame update
    private void Awake ()
    {
        instance = this;
    }

    void Start()
    {
        animator = GetComponent<Animator> ();
        playercurrentHP = playerHP;
        enemycurrentHP = enemyHP;

        enemy.player = this;

        gameover = false;

        
    }

    // Update is called once per frame
    void Update()
    {
        if ( !gameover )
        {
            parrywindowmultiplier += multgainrate * Time.deltaTime;
            if ( parrywindowmultiplier <= 1 )
            {

            }
            else
                parrywindowmultiplier = 1;

            if ( Input.GetMouseButtonDown ( 0 ) )
            {
                blocking = true;
                animator.SetBool ( "block", true );

                checkparry ();
            }

            if ( Input.GetMouseButtonUp ( 0 ) )
            {
                blocking = false;
                animator.SetBool ( "block", false );
            }

            if ( enemycurrentHP <= 0 )
            {
                gameover = true;

                enemy.die ();

                Invoke ( "restartgame", 3f );
            }

            if ( playercurrentHP <= 0 )
            {
                gameover = true;

                die ();
                enemy.win ();

                Invoke ( "restartgame", 3f );
            }
        }
        

        enemyhealth.fillAmount = enemycurrentHP / enemyHP;
        playerhealth.fillAmount = playercurrentHP / playerHP;

        
    }

    void die ()
    {
        animator.SetBool ( "die",true );
        blocking = false;
        animator.SetBool ( "block", false );
    }



    void checkparry ()
    {
        if ( attackwindow )
        {
            enemy.gethit ();
            enemycurrentHP -= playerDmg;
            animator.SetTrigger ( "counter" );
            Debug.Log ( "countered attack" );
            attackwindow = false;
            Time.timeScale = 1;

            parrywindowmultiplier = 1;
            timer.resettimer ();
        }
        else
        {
            parrywindowmultiplier *= multmultiplyer;
        }
    }

    public void restartgame ()
    {
        SceneManager.LoadScene ( SceneManager.GetActiveScene ().buildIndex );
        
    }
}
