﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arifscript : MonoBehaviour
{
    float a, b, c, m, n;
    public float R, P, S;
    public float initA, initB, initC, ininM, initN;
    // Start is called before the first frame update
    void Start()
    {
        a = initA-1; b = initB-1; c = initC-1; m = ininM-1; n = initN-1;

        float newA = a + 1;
        float newB = b + 1;
        float newC = c + 1;
        float newM = m + 1;
        float newN = n + 1;

        int numberiterations = 0;

        while (!( ( Mathf.Abs ( newA - a ) < 0.00001f ) && ( Mathf.Abs ( newB - b ) < 0.00001f ) && ( Mathf.Abs ( newC - c ) < 0.00001f ) && ( Mathf.Abs ( newM - m ) < 0.00001f ) && ( Mathf.Abs ( newN - n ) < 0.00001f ) ))
        {
            a = newA; b = newB; c = newC; m = newM; n = newN;

            //Matrix<float> Jacobian = Matrix<float>.Build.Dense ( 5, 5, ( i, j ) => jacobian5x5element ( j + 1, i + 1 ) );
            //Vector<float> originalfunction = Vector<float>.Build.Dense ( 5, ( i ) => -1 * function5 ( i + 1 ) );

            //var solution = Jacobian.Solve ( originalfunction );

            //newA = a + solution.At ( 0 );
            //newB = b + solution.At ( 1 );
            //newC = c + solution.At ( 2 );
            //newM = m + solution.At ( 3 );
            //newN = n + solution.At ( 4 );

            numberiterations++;
        }

        Debug.Log ( "A = " + a + " , B = " + b + " , C = " + c + " , M = " + m + " , N = " + n + " , number of iterations required = " + numberiterations );
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    float jacobian5x5element ( int row, int column )
    {
        float f = 0;

        switch ( row )
        {
            case 1:
                switch ( column )
                {
                    case 1:
                        f = R * R / 2;
                        break;
                    case 2:
                        f = R / Mathf.Sqrt ( 2 );
                        break;
                    case 3:
                        f = 1;
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                }
                break;
            case 2:
                switch ( column )
                {
                    case 1:
                        f = R * Mathf.Sqrt ( 2 );
                        break;
                    case 2:
                        f = 1;
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                }
                break;
            case 3:
                switch ( column )
                {
                    case 1:
                        f = m * m;
                        break;
                    case 2:
                        f = m;
                        break;
                    case 3:
                        f = 1;
                        break;
                    case 4:
                        f = 2 * a * m + b;
                        break;
                    case 5:
                        f = -1;
                        break;
                }
                break;
            case 4:
                switch ( column )
                {
                    case 1:
                        f = 2 * m * m - 2 * P * m;
                        break;
                    case 2:
                        f = m - P;
                        break;
                    case 3:
                        break;
                    case 4:
                        f = 4 * a * m - 2 * a * P + b;
                        break;
                    case 5:
                        f = 1;
                        break;
                }
                break;
            case 5:
                switch ( column )
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        f = 2 * ( m - P );
                        break;
                    case 5:
                        f = 2 * n;
                        break;
                }
                break;
        }

        

        return f;
    }

    float function5 ( int row )
    {
        float f = 0;

        switch ( row )
        {
            case 1:
                f = ( R * R * a / 2f ) + ( ( b - 1 ) * R / Mathf.Sqrt ( 2 ) ) + c ;
                break;
            case 2:
                f = ( Mathf.Sqrt ( 2 ) * R * a ) + b + 1;
                break;
            case 3:
                f = (a * m * m) + (b * m) - n + c;
                break;
            case 4:
                f = (2*a*m*m) - (2*a*P*m) + (m*b) + n - (b*P);
                break;
            case 5:
                f = (m-P)*(m-P) + n*n - S*S;
                break;
        }

        return f;
    }
}
