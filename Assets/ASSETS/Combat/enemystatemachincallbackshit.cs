﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemystatemachincallbackshit : StateMachineBehaviour
{


    override public void OnStateEnter ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
    }

    override public void OnStateExit ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        if ( PlayerCombat.instance.enemy )
        {
            PlayerCombat.instance.enemy.hitfinished ( animator, stateInfo, layerIndex );
        }
    }

    override public void OnStateUpdate ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        
    }

    override public void OnStateMove ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {

    }

    override public void OnStateIK ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {

    }
}
