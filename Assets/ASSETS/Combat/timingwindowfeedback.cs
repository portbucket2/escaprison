﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timingwindowfeedback : MonoBehaviour
{
    public Image dial;

    public Image disc;
    public Image discbg;

    RectTransform discrect;
    RectTransform parentrect;

    float parentscale;
    float initparentscale;

    float timeremaining;
    float fulltime=1;

    float timeperhundredunit = 0.1f;

    float fullparrytime;
    // Start is called before the first frame update
    void Start()
    {
        fullparrytime = 0.5f;
        //dial = GetComponent<Image> ();
        if(disc)discrect = disc.GetComponent<RectTransform> ();
        if(discbg)parentrect = discbg.GetComponent<RectTransform> ();

        initparentscale = parentrect.rect.width;
    }

    // Update is called once per frame
    void Update()
    {
        float fullscale = fullparrytime * PlayerCombat.instance.parrywindowmultiplier * 100 / timeperhundredunit;
        if ( timeremaining > 0 )
        {
            timeremaining -= Time.deltaTime;
            discbg.enabled = false;
        }
        else
        {
            timeremaining = 0;

            if(parentrect)parentrect.sizeDelta =Vector2.one * fullscale;
            discbg.enabled = true;
        }

        if(discrect)discrect.sizeDelta = (timeremaining * 100 / timeperhundredunit)*Vector2.one;

        if(dial)dial.fillAmount = timeremaining / fulltime;

        if (! PlayerCombat.instance.showripostetiming )
        {
            if ( parentrect )
                parentrect.sizeDelta = Vector2.one * 0;
            if ( discrect )
                discrect.sizeDelta = 0 * Vector2.one;

            if ( dial )
                dial.fillAmount = 0;
        }
    }

    public void starttimer ( float time )
    {
        timeremaining = time;
        fulltime = time;
    }

    public void setnewparrytime ( float parrytime )
    {
        fullparrytime = parrytime;
    }

    public void resettimer ()
    {
        timeremaining = 0;
    }
}
