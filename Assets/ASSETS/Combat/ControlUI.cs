﻿using FRIA;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlUI : MonoBehaviour
{
    public Slider ParryLossPerClick; 
    public Slider ParryGainPerSec; 
    public Slider ParryOverrideTiming;

    public Text ParryLossPerClickDescription;
    public Text ParryGainPerSecDescription;
    public Text ParryOverrideTimingDescription;

    public Text EnemyDamageDescription;
    public Text PlayerDamageDescription;

    public Toggle ParryTimingFeedback;
    public Toggle SlowMo;
    public Toggle counterSlowMo;
    public Toggle ParryTimingOverride;

    public InputField EnemyDamage;
    public InputField PlayerDamage;
    public InputField EnemyHP;
    public InputField PlayerHP;

    public Button restartbutton;
    public Button resetbutton;


    public HardData<float> ParryLossPerClick_Persistent;
    public HardData<float> ParryGainPerSec_Persistent;
    public HardData<float> ParryOverrideTiming_Persistent;
    public HardData<float> EnemyDamage_Persistent;
    public HardData<float> PlayerDamage_Persistent;

    public HardData<bool> ParryTimingFeedback_Persistent;
    public HardData<bool> SlowMo_Persistent;
    public HardData<bool> counterSlowMo_Persistent;
    public HardData<bool> ParryTimingOverride_Persistent;

    public HardData<bool> ResetData;



    // Start is called before the first frame update
    void Start()
    {

        PlayerCombat plc = PlayerCombat.instance;

        ResetData = new HardData<bool> ( "RESET_ALL_DATA", true );

        if ( restartbutton )
        {
            restartbutton.onClick.AddListener ( plc.restartgame );
        }

        if ( resetbutton )
        {
            resetbutton.onClick.AddListener ( ()=>ResetData.value=true );
        }

        ParryTimingFeedback_Persistent = new HardData<bool> ( "PARRY_TIMING_FEEDBACK", plc.showripostetiming );
        SlowMo_Persistent = new HardData<bool> ( "SLOWMO", plc.slowmo );
        counterSlowMo_Persistent = new HardData<bool> ( "COUNTER_SLOWMO", counterSlowMo );
        ParryTimingOverride_Persistent = new HardData<bool> ( "CUSTOM_PARRY_TIMING_OVERRIDE", ParryTimingOverride );
        ParryLossPerClick_Persistent = new HardData<float> ( "PARRY_LOSS/CLICK", plc.multmultiplyer );
        ParryGainPerSec_Persistent = new HardData<float> ( "PARRY_GAIN/SEC", plc.multgainrate );
        ParryOverrideTiming_Persistent = new HardData<float> ( "OVERRIDEN_PARRY_TIMING", plc.parrywindow );
        EnemyDamage_Persistent = new HardData<float> ( "ENEMY_DAMAGE", plc.enemyDmg );
        PlayerDamage_Persistent = new HardData<float> ( "PLAYER_DAMAGE", plc.playerDmg );

        if ( ResetData.value )
        {
            ParryLossPerClick.value = plc.multmultiplyer;
            ParryGainPerSec.value = plc.multgainrate;
            ParryOverrideTiming.value = plc.parrywindow;

            ParryTimingFeedback.isOn = plc.showripostetiming;
            SlowMo.isOn = plc.slowmo;
            counterSlowMo.isOn = plc.counterslowmo;
            ParryTimingOverride.isOn = plc.overrideparrywindow;

            EnemyDamage.text = plc.enemyDmg.ToString ();
            PlayerDamage.text = plc.playerDmg.ToString ();

            ResetData.value = false;
        }
        else
        {
            ParryLossPerClick.value = plc.multmultiplyer = ParryLossPerClick_Persistent.value;
            ParryGainPerSec.value = plc.multgainrate = ParryGainPerSec_Persistent.value;
            ParryOverrideTiming.value = plc.parrywindow = ParryOverrideTiming_Persistent.value;

            ParryTimingFeedback.isOn = plc.showripostetiming = ParryTimingFeedback_Persistent.value;
            SlowMo.isOn = plc.slowmo = SlowMo_Persistent.value;
            counterSlowMo.isOn = plc.counterslowmo = counterSlowMo_Persistent.value;
            ParryTimingOverride.isOn = plc.overrideparrywindow = ParryTimingOverride_Persistent.value;

            plc.enemyDmg = EnemyDamage_Persistent.value;
            plc.playerDmg = PlayerDamage_Persistent.value;

            EnemyDamage.text = plc.enemyDmg.ToString ();
            PlayerDamage.text = plc.playerDmg.ToString ();

        }

        

        ParryLossPerClickDescription.text = "ParryLoss/Click = " + ParryLossPerClick.value;
        ParryGainPerSecDescription.text = "ParryGain/Sec = " + ParryGainPerSec.value;
        ParryOverrideTimingDescription.text = "Overriden Parry Time = " + ParryOverrideTiming.value;

        

        

        EnemyDamageDescription.text = "Enemy Damage = " + plc.enemyDmg.ToString ();
        PlayerDamageDescription.text = "Player Damage = " + plc.playerDmg.ToString ();

        ParryLossPerClick.onValueChanged.AddListener ( ( value ) =>
        {
            plc.multmultiplyer = value;
            ParryLossPerClick_Persistent.value = value;
            ParryLossPerClickDescription.text = "ParryLoss/Click = " + value;
        } );

        ParryGainPerSec.onValueChanged.AddListener ( ( value ) =>
        {
            plc.multgainrate = value;
            ParryGainPerSec_Persistent.value = value;
            ParryGainPerSecDescription.text = "ParryGain/Sec = " + value;
        } );

        ParryOverrideTiming.onValueChanged.AddListener ( ( value ) =>
        {
            plc.parrywindow = value;
            ParryOverrideTiming_Persistent.value = value;
            ParryOverrideTimingDescription.text = "Overriden Parry Time = " + value;
        } );

        ParryTimingFeedback.onValueChanged.AddListener ( ( value ) =>
        {
            plc.showripostetiming = value;
            ParryTimingFeedback_Persistent.value = value;
        } );

        SlowMo.onValueChanged.AddListener ( ( value ) =>
        {
            plc.slowmo = value;
            SlowMo_Persistent.value = value;
        } );

        counterSlowMo.onValueChanged.AddListener ( ( value ) =>
        {
            plc.counterslowmo = value;
            counterSlowMo_Persistent.value = value;
        } );

        ParryTimingOverride.onValueChanged.AddListener ( ( value ) =>
        {
            plc.overrideparrywindow = value;
            ParryTimingOverride_Persistent.value = value;
        } );

        EnemyDamage.onValueChanged.AddListener((value)=>
        {
            float.TryParse ( value, out float f );
            plc.enemyDmg = f;
            EnemyDamage_Persistent.value = f;
            EnemyDamageDescription.text = "Enemy Damage = " + value;
        } );

        PlayerDamage.onValueChanged.AddListener ( ( value ) =>
        {
            float.TryParse ( value, out float f );
            plc.playerDmg = f;
            PlayerDamage_Persistent.value = f;
            PlayerDamageDescription.text = "Player Damage = " + value;
        } );
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
