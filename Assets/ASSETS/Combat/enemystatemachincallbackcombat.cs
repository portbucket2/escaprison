﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemystatemachincallbackcombat : StateMachineBehaviour
{


    override public void OnStateEnter ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        PlayerCombat.instance.counterstarted ( animator, stateInfo );
    }

    override public void OnStateExit ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        PlayerCombat.instance.counterended ( animator, stateInfo );
    }

    override public void OnStateUpdate ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        
    }

    override public void OnStateMove ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {

    }

    override public void OnStateIK ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {

    }
}
