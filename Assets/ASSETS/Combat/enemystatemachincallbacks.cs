﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemystatemachincallbacks : StateMachineBehaviour
{
    bool alreadycalledattack = false;
    
    public float parrywindow = 0.3f;


    override public void OnStateEnter ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        parrywindow = PlayerCombat.instance.overrideparrywindow? PlayerCombat.instance.parrywindow : 0.5f;

        PlayerCombat.instance.newparrytimeset ( parrywindow );
    }

    override public void OnStateExit ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        
        if ( PlayerCombat.instance.enemy )
        {
            if ( !PlayerCombat.instance.enemy.interrupted )
            {
                PlayerCombat.instance.AttackOccured ( animator, stateInfo );
                PlayerCombat.instance.enemy.attacked ( animator, stateInfo, layerIndex );
            }
        }
        alreadycalledattack = false;
    }

    override public void OnStateUpdate ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        float normalizedtime = stateInfo.normalizedTime - ( (int) stateInfo.normalizedTime );

        float windowfraction = PlayerCombat.instance.parrywindowmultiplier * parrywindow / stateInfo.length;

        if ( normalizedtime > ( 1 - windowfraction ) )
        {
            if ( !alreadycalledattack )
            {
                PlayerCombat.instance.AttackWindowOpen ( animator,stateInfo,(1-normalizedtime)*stateInfo.length );
                
                alreadycalledattack = true;
            }
        }
    }

    override public void OnStateMove ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {

    }

    override public void OnStateIK ( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {

    }
}
