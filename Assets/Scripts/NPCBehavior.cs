﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehavior : MonoBehaviour
{
    [SerializeField] LinkedPoint origin;
    [SerializeField] LinkedPoint firstdestination;
    LinkedPoint destination;
    LinkedPoint next;
    [SerializeField] float minDistDelta = 0.01f;
    [SerializeField] Animator anim;

    public float difficulty = 0.5f;

    CharController player;

    public bool playermoving = true;

    public List<int> turnchoices;

    public float steadytime = 5;

    public int turnindex;
    // Start is called before the first frame update
    void Start()
    {
        destination = firstdestination;
        transform.position = origin.transform.position;
        prevpos = transform.position - 0.01f * ( destination.transform.position - origin.transform.position ).normalized;

        player = CharController.instance;
        anim.SetBool ( "patrol", playermoving );
    }

    // Update is called once per frame
    void Update()
    {
        if ( CharController.instance.holdallbehavior )
        {
            anim.SetBool ( "patrol", false );
        }
        else
        {
            anim.SetBool ( "patrol", playermoving );
            if ( playermoving )
            {
                progressTowardsDestination ();
            }
            else
            {
                sentrybehavior ();
            }

            transform.rotation = Quaternion.Slerp ( transform.rotation, Quaternion.LookRotation ( transform.position - prevpos, Vector3.up ), 0.5f );

            checkdetection = detect ();

            if ( ( player.transform.position - transform.position ).magnitude < attackrange )
            {
                if ( checkdetection )
                {
                    player.IkillU ( this );
                }
                else
                {
                    player.killmepls ( this );
                }
            }
        }

        

        
    }
    Vector3 prevpos;
    [SerializeField] float movementspeed = 3;

    void progressTowardsDestination ()
    {
        
        if ( turnchoices.Count > 0 )
        {
            if ( ( transform.position - destination.transform.position ).magnitude > minDistDelta )
            {



                prevpos = transform.position;
                transform.position = Vector3.MoveTowards ( transform.position, destination.transform.position, movementspeed * Time.deltaTime );
            }
            else
            {
                if ( turnchoices[turnindex] >= 0 )
                {
                    ChooseDirection ( (Direction) turnchoices[turnindex] );
                }
                else
                {
                    flip ();
                }

                origin = destination;
                destination = next;

                turnindex++;
                if ( turnindex >= turnchoices.Count )
                {
                    turnindex = 0;
                }
            }
        }
    }

    float tm = 0;

    void sentrybehavior ()
    {
        tm += Time.deltaTime;

        if ( tm > steadytime )
        {
            tm = 0;

            changesweep ();
        }

    }

    void changesweep ()
    {
        if ( turnchoices.Count > 0 )
        {
            int choice = turnchoices[turnindex];

            LinkedPoint[] points = origin.GetRelativePoint ( destination );

            if ( points!=null )
            {
                if ( choice < 0 )
                {
                    if ( points[1] )
                    {
                        destination = points[1];

                        prevpos = transform.position - 0.01f * (destination.transform.position - origin.transform.position).normalized;
                    }
                }
                else if(choice==0)
                {
                    if ( points[2] )
                    {
                        destination = points[2];

                        prevpos = transform.position - 0.01f * ( destination.transform.position - origin.transform.position ).normalized;
                    }
                }
                else if ( choice == 2 )
                {
                    if ( points[0] )
                    {
                        destination = points[0];

                        prevpos = transform.position - 0.01f * ( destination.transform.position - origin.transform.position ).normalized;
                    }
                }
                else if ( choice == 1 )
                {
                }
            }

            turnindex++;
            if ( turnindex >= turnchoices.Count )
            {
                turnindex = 0;
            }
        }
    }

    

    bool ChooseDirection (Direction d)
    {
        LinkedPoint[] points = destination.GetRelativePoint ( origin );

        if ( points != null )
        {
            //Debug.Log ( "point 0 is null?  " + ( points[0] == null ).ToString () );
            //Debug.Log ( "point 1 is null?  " + ( points[1] == null ).ToString () );
            //Debug.Log ( "point 2 is null?  " + ( points[2] == null ).ToString () );
            if ( d == Direction.FRONT )
            {
                if ( points[1] )
                {
                    next = points[1];

                    return true;
                }
                else if ( points[2] )
                {
                    next = points[2];
                    return true;
                }
                else if ( points[0] )
                {
                    next = points[0];
                    return true;
                }
                else
                {
                    next = null;
                }
            }
            else if ( d == Direction.RIGHT )
            {
                if ( points[2] )
                {
                    next = points[2];
                    return true;
                }
                else if ( points[1] )
                {
                    next = points[1];
                    return true;
                }
                else if ( points[0] )
                {
                    next = points[0];
                    return true;
                }
                else
                {
                    next = null;
                }
            }
            else
            {
                if ( points[0] )
                {
                    next = points[0];
                    return true;
                }
                else if ( points[1] )
                {
                    next = points[1];
                    return true;
                }
                else if ( points[2] )
                {
                    next = points[2];
                    return true;
                }
                else
                {
                    next = null;
                }
            }
        }

        return false;
    }

    void flip ()
    {
        next = origin;
    }

    [SerializeField]float detectionRange = 5;
    [SerializeField]float closenessrange = 1;
    [SerializeField]float attackrange = 0.5f;

    [SerializeField] bool checkdetection;

    bool detect ()
    {
        float range = detectionRange;

        float checkingDistance = ( transform.position - destination.transform.position ).magnitude;

        List<LinkedPoint> frontpoints = new List<LinkedPoint> ();

        LinkedPoint frontpoint = destination;
        LinkedPoint backpoint = origin;

        

        LinkedPoint nextdest = frontpoint.GetRelativePoint ( backpoint )?[1];

        frontpoints.Add ( frontpoint );

        while ( nextdest != null && checkingDistance < range )
        {


            backpoint = frontpoint;
            frontpoint = nextdest;

            frontpoints.Add ( frontpoint );

            range -= checkingDistance;
            checkingDistance = ( frontpoint.transform.position - backpoint.transform.position ).magnitude;

            nextdest = frontpoint.GetRelativePoint ( backpoint )?[1];
        }

        bool playerfound = false;



        if ( nextdest != null )
        {

            // check till range from second last point
            if ( frontpoints.Count > 1 )
            {
                for ( int i = 0; i < frontpoints.Count - 2; i++ )
                {
                    if ( player.ISPlayerNearHere ( frontpoints[i], closenessrange ) )
                    {
                        playerfound = true;


                        Debug.Log ( "Proximity Check made for Multiple points. Range DID NOT hit a wall. Player found near frontpoint  " + i );
                        return playerfound;

                    }
                }

                for ( int i = frontpoints.Count - 1; i > 0; i-- )
                {
                    float playerpos = player.CheckPlayerPlacementBetweenPoints ( frontpoints[i - 1], frontpoints[i] );

                    if ( i == frontpoints.Count - 1 )
                    {
                        if ( playerpos >= 0 && playerpos < range )
                        {
                            //found player
                            playerfound = true;

                            Debug.Log ( "Sandwich Check made for Multiple points(except first point). Range DID NOT hit a wall. Player found before frontpoint  " + i +" . This be last point, so player also within range of last point" );
                            return playerfound;
                        }
                        else
                        {
                            // did not find
                        }
                    }
                    else
                    {
                        if ( playerpos >= 0 )
                        {
                            // found player
                            playerfound = true;

                            Debug.Log ( "Sandwich Check made for Multiple points(except first point). Range DID NOT hit a wall. Player found before frontpoint  " + i  );
                            return playerfound;
                        }
                        else
                        {
                            //did not find
                        }
                    }
                }
                float ppos = player.CheckPlayerPlacementBetweenPoints ( origin, frontpoints[0] );
                float npcpos = ( transform.position - origin.transform.position ).magnitude;
                if ( ppos >= 0 && ppos > npcpos )
                {
                    // found player
                    playerfound = true;

                    Debug.Log ( "Sandwich Check made for Multiple points(first point). Range DID NOT hit a wall. Player found here  ");
                    return playerfound;
                }
                else
                {
                    //did not find
                }


            }
            else
            {
                float playerpos = player.CheckPlayerPlacementBetweenPoints ( origin, destination );
                float npcpos = ( transform.position - origin.transform.position ).magnitude;
                if ( playerpos >= 0 && playerpos > npcpos && playerpos < ( npcpos + range ) )
                {
                    //found player
                    playerfound = true;

                    Debug.Log ( "Sandwich Check made for Single point. Range DID NOT hit a wall. Player found here  " );
                    return playerfound;
                }
                else
                {
                    // no player
                }
            }
        }
        else //hit a wall
        {


            if ( frontpoints.Count > 1 )
            {
                for ( int i = 0; i < frontpoints.Count - 2; i++ )
                {
                    if ( player.ISPlayerNearHere ( frontpoints[i], closenessrange ) )
                    {
                        playerfound = true;

                        Debug.Log ( "Proximity Check made for Multiple points(except last point). Range may have hit a wall. Player found near frontpoint  " + i );
                        return playerfound;
                    }
                }

                for ( int i = frontpoints.Count - 1; i > 0; i-- )
                {
                    float playerpos = player.CheckPlayerPlacementBetweenPoints ( frontpoints[i - 1], frontpoints[i] );

                    if ( i == frontpoints.Count - 1 )
                    {
                        if ( checkingDistance < range )
                        {
                            // range is farther than wall. check only up to wall

                            if ( player.ISPlayerNearHere ( frontpoints[i], closenessrange ) )
                            {
                                playerfound = true;

                                Debug.Log ( "Proximity Check made for last point. Range hit a wall. Player found near frontpoint  " + i );
                                return playerfound;
                            }
                            else
                            {
                                //did not find
                            }

                            if ( playerpos >= 0 )
                            {
                                //found player
                                playerfound = true;

                                Debug.Log ( "Sandwich Check made for Multiple points(except first point). Range hit a wall, so checked till the wall. Player found before frontpoint  " + i + " . This be last point, so player also within range of last point" );
                                return playerfound;
                            }
                            else
                            {
                                // did not find
                            }
                        }
                        else
                        {
                            // range is closer than wall. check till range

                            if ( playerpos >= 0 && playerpos < range )
                            {
                                //found player
                                playerfound = true;

                                Debug.Log ( "Sandwich Check made for Multiple points(except first point). Range may not have hit a wall, so checked till the end of range. Player found before frontpoint  " + i + " . This be last point, so player also within range of last point" );
                                return playerfound;
                            }
                            else
                            {
                                // did not find
                            }
                        }


                    }
                    else
                    {

                        if ( playerpos >= 0 )
                        {
                            // found player
                            playerfound = true;

                            Debug.Log ( "Sandwich Check made for Multiple points(except first point). Range hitting wall is irrelevant, since this isn't last point. Player found before frontpoint  " + i  );
                            return playerfound;
                        }
                        else
                        {
                            //did not find
                        }
                    }
                }
                float ppos = player.CheckPlayerPlacementBetweenPoints ( origin, frontpoints[0] );
                float npcpos = ( transform.position - origin.transform.position ).magnitude;
                if ( ppos >= 0 && ppos > npcpos )
                {
                    // found player
                    playerfound = true;

                    Debug.Log ( "Sandwich Check made for Multiple points(first point). Range hitting wall is irrelevant, since this isn't last point. Player found before here");
                    return playerfound;
                }
                else
                {
                    //did not find
                }


            }
            else
            {
                float playerpos = player.CheckPlayerPlacementBetweenPoints ( origin, destination );
                float npcpos = ( transform.position - origin.transform.position ).magnitude;
                if ( checkingDistance < range )
                {
                    if ( player.ISPlayerNearHere ( destination, closenessrange ) )
                    {
                        playerfound = true;

                        Debug.Log ( "Proximity Check made for single point.  Range hit a wall. Player found near this point  " );
                        return playerfound;
                    }
                    else
                    {
                        //did not find player
                    }

                    if ( playerpos >= 0 && playerpos > npcpos )
                    {
                        //found player
                        playerfound = true;

                        Debug.Log ( "Sandwich Check made for single point.  Range hit a wall. Player found here  " );
                        return playerfound;
                    }
                    else
                    {
                        // no player
                    }
                }
                else
                {
                    if ( playerpos >= 0 && playerpos > npcpos && playerpos < ( npcpos + range ) )
                    {
                        //found player
                        playerfound = true;

                        Debug.Log ( "Sandwich Check made for single point.  Range may not have hit a wall. Player found here  " );
                        return playerfound;
                    }
                    else
                    {
                        // no player
                    }
                }

            }
        }

        return playerfound;
    }

    public void die ()
    {
        gameObject.SetActive ( false );
    }

    
}
